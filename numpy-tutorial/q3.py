import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

img = plt.imread("lena.png")
darker_img = deepcopy(img)
lighter_img = deepcopy(img)

for i in range(0, img.shape[0]) :
    for j in range(0, img.shape[1]) :
        for k in range(0, img.shape[2]) :
            lighter_img[i][j][k] = img[i][j][k] * 1.9 if img[i][j][k] * 1.9 <= 1 else 1


for i in range(0, img.shape[0]) :
    for j in range(0, img.shape[1]) :
        for k in range(0, img.shape[2]) :
            darker_img[i][j][k] = img[i][j][k] * 0.5

# Plotando a imagem resultante junto com a imagem original
fig = plt.figure()
ax1 = fig.add_subplot(1,3,1)
ax1.imshow(darker_img)
ax2 = fig.add_subplot(1,3,2)
ax2.imshow(img)
ax3 = fig.add_subplot(1,3,3)
ax3.imshow(lighter_img)
plt.show()
