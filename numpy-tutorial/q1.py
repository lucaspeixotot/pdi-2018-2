import numpy as np
import matplotlib.pyplot as plt

# Criando a imagem preta
img1 = np.zeros([400, 400, 3], dtype=np.uint8)

# Criando a imagem cinza (128,128,128)
img2 = np.zeros([400, 400, 3], dtype=np.uint8)
img2.fill(128)

# Plotando as imagens utilizando o matplotlib
fig = plt.figure()
ax1 = fig.add_subplot(1,2,1)
ax1.imshow(img1)
ax2 = fig.add_subplot(1,2,2)
ax2.imshow(img2)
plt.show()
