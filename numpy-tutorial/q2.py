import numpy as np
from random import randint
import matplotlib.pyplot as plt

# Lendo a imagem
img = plt.imread("baboon.png")
#%%
# Limitando a imagem em x
lmin = 230
lmax = 480
x_shape = lmax - lmin

# Criando uma imagem com o X limitado que será usada futuramente
aux_img = img[lmin:lmax]

# Limitando a imagem em y
lmin = 120
lmax = 360
y_shape = lmax - lmin

# Criando o array que vai conter a imagem resultante
img_result = np.zeros([x_shape, y_shape, 3], dtype = np.float64)

# Preenchendo o array
for i in range(0, aux_img.shape[0]) :
    img_result[i] = aux_img[i][lmin:lmax]

# Plotando a imagem resultante junto com a imagem original
fig = plt.figure()
ax1 = fig.add_subplot(1,2,1)
ax1.imshow(img)
ax1.set_title('Antes')
ax2 = fig.add_subplot(1,2,2)
ax2.set_title('Resultado')
ax2.imshow(img_result)
plt.show()
