# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import cv2
from pdi_lib import load_img, uint_to_float, plot_img_cv2, normalize, log_transformation

#%% Dando load na imagem
img = load_img('imgs/campo.png', 1)


#%% Gerando o histograma da imagem original

plt.figure()
plt.hist(img.flatten(),256,[0,256], color = 'r')
plt.xlim([0,256])
plt.legend(('cdf','histogram'), loc = 'upper left')
plt.show()

#%% Equalizando o historgama dela

equ = cv2.equalizeHist(img)

#%% Plotando o histograma da imagem equalizada
plt.figure()
plt.hist(equ.flatten(),256,[0,256], color = 'r')
plt.xlim([0,256])
plt.legend(('cdf','histogram'), loc = 'upper left')
plt.show()

#%% Mostrando o resultado
res = np.hstack((img, equ))
plot_img_cv2(res)


#%%
img_log = log_transformation(img, 1.0)
res = np.hstack((uint_to_float(img), img_log))
plot_img_cv2(res)