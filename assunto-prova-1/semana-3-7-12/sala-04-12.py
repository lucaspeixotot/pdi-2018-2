# -*- coding: utf-8 -*-
#%%
import numpy as np
import matplotlib.pyplot as plt
import cv2
from pdi_lib import sum_imgs, max_operator_img_rgb, diff_absolut, normalize, saturation, rotation, plot_img_cv2

#%%
baboon = cv2.imread("imgs/baboon.png")
lena = cv2.imread("imgs/lena.png")

#%% Somando as imagens e plotando
ex1 = sum_imgs(baboon, lena, 0.5, 0.5)

cv2.imshow('img', ex1)
cv2.waitKey(0)
cv2.destroyAllWindows()

while True :
    if (cv2.waitKey(1 == int('q'))): break
        
#%% Operador maximo e plotando

ex2 = max_operator_img_rgb(baboon, lena)

cv2.imshow('img', ex2)
cv2.waitKey(0)
cv2.destroyAllWindows()

while True :
    if (cv2.waitKey(1 == int('q'))): break

#%% Diferença absoluta e plotando

ex3 = diff_absolut(baboon, lena)

cv2.imshow('img', ex3)
cv2.waitKey(0)
cv2.destroyAllWindows()

while True :
    if (cv2.waitKey(1 == int('q'))): break

#%% Rotação
plot_img_cv2(rotation(lena, 45))

#%% Zooming
res = cv2.resize(baboon,None,fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
plt.imshow(cv2.cvtColor(res, cv2.COLOR_BGR2RGB))
plt.show()