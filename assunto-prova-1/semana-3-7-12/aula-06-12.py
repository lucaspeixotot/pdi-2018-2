# -*- coding: utf-8 -*-
#%%
import numpy as np
import matplotlib.pyplot as plt
import cv2
from pdi_lib import plot_img_cv2, and_img, or_img, xor_img, not_img, load_img, max_operator_img_gray

#%%
baboon = load_img("../imgs/baboon.png")
lena = load_img("../imgs/lena.png")
circle1 = load_img("../imgs/circle1.png")
circle2 = load_img("../imgs/circle2.png")
square1 = load_img("../imgs/square1.png", 1)
square2 = load_img("../imgs/square2.png", 1)
baboon = cv2.cvtColor(baboon,cv2.COLOR_BGR2GRAY)
lena = cv2.cvtColor(lena,cv2.COLOR_BGR2GRAY)

#%% ex1

fig = plt.figure()
ax1 = fig.add_subplot(2,2,1)
ax1.imshow(circle1, cmap='gray')
ax1.set_title('Imagem inicial')
plt.axis('off')
ax2 = fig.add_subplot(2,2,2)
ax2.imshow(circle2, cmap='gray')
ax2.set_title('Imagem final')
plt.axis('off')
ax3 = fig.add_subplot(2,2,3)
ax3.imshow(circle1 - circle2, cmap='gray')
ax3.set_title('Perspectiva de movimento em relação a imagem inicial')
plt.axis('off')
ax4 = fig.add_subplot(2,2,4)
ax4.imshow(circle2 - circle1, cmap='gray')
ax4.set_title('Perspectiva de movimento em relação a imagem final')
plt.axis("off")
plt.show()

#%% ex2
fig = plt.figure()
ax1 = fig.add_subplot(4,2,1)
ax1.imshow(square1, cmap='gray')
ax1.set_title("Imagem 1")
plt.axis("off")
ax2 = fig.add_subplot(4,2,2)
ax2.imshow(square2, cmap='gray')
ax2.set_title("Imagem 2")
plt.axis("off")
ax3 = fig.add_subplot(4,2,3)
ax3.imshow(and_img(square1, square2), cmap='gray')
ax3.set_title("And op")
plt.axis("off")
ax4 = fig.add_subplot(4,2,4)
ax4.imshow(or_img(square1, square2), cmap='gray')
ax4.set_title("Or op")
plt.axis("off")
ax5 = fig.add_subplot(4,2,5)
ax5.imshow(xor_img(square1, square2), cmap='gray')
ax5.set_title("Xor op")
plt.axis("off")
ax6 = fig.add_subplot(4,2,6)
ax6.imshow(not_img(square1), cmap='gray')
ax6.set_title("Not op img 1")
plt.axis("off")
plt.tight_layout(pad=0.2)
plt.show()

#%% ex3
plt.imshow(max_operator_img_gray(baboon, lena), cmap='gray')
plt.axis("off")
plt.show()