# -*- coding: utf-8 -*-
#%%
import numpy as np
import matplotlib.pyplot as plt
import cv2


#%%
def uint_to_float(img) :
    return saturation(normalize(img))
#%%
def normalize(img) :
    return img / 255.

#%% Saturar

def saturation(img) :
    return np.where(img > 1., 1., img)

#%% Sum imgs function
def sum_imgs(img1, img2, coef1, coef2) :
    img1 = img1 * coef1
    img2 = img2 * coef2
    res = (img1 + img2)
    res = saturation(normalize(res))
    return res

#%% Max operator img rgb
# np.maximum faz a msm coisa
def max_operator_img_rgb(img1, img2):
    if (img1.shape != img2.shape) : 
        raise Exception('Tamanho das imagens não é igual.')
    x_range, y_range, z_range = img1.shape
    new_img = np.zeros([img1.shape[0], img1.shape[1], 3], dtype=np.uint8)
    for x in range(x_range) :
        for y in range(y_range) :
            for z in range(z_range) :
                maximum = img1[x][y][z]                
                if maximum < img2[x][y][z] :
                    maximum = img2[x][y][z]
                new_img[x][y][z] = maximum
    return new_img

#%% Max operator img grayscale
# np.maximum faz a msm coisa
def max_operator_img_gray(img1, img2):
    if (img1.shape != img2.shape) : 
        raise Exception('Tamanho das imagens não é igual.')
    x_range, y_range = img1.shape
    new_img = np.zeros([img1.shape[0], img1.shape[1]], dtype=np.uint8)
    for x in range(x_range) :
        for y in range(y_range) :
            maximum = img1[x][y]                
            if maximum < img2[x][y] :
                maximum = img2[x][y]
            new_img[x][y] = maximum
    return new_img


#%% Diff absolut

def diff_absolut(img1, img2) :
    new_img = img1 - img2
    new_img = np.where(new_img < 0, -new_img, new_img)
    return new_img
    
#%% Negative function

def negative_img(img) :
    return 1. - img
    
#%% plot_img
    
def plot_img_cv2(img) :
    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
            
#%% and img
            
def and_img(img1, img2) :
    if (img1.shape != img2.shape) : 
        raise Exception('Tamanho das imagens não é igual.')
    return img1 & img2

def or_img(img1, img2):
    if (img1.shape != img2.shape) : 
        raise Exception('Tamanho das imagens não é igual.')
    return img1 | img2

def xor_img(img1, img2) :
    if (img1.shape != img2.shape) : 
        raise Exception('Tamanho das imagens não é igual.')
    return img1 ^ img2

def not_img(img) :
    return 255 - img

#%% load image
def load_img(path, grayscale=0) :
    if grayscale :
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    else :
        img = cv2.imread(path)
    return img

#%% Rotation

def rotation(img, degrees) :  
    rows,cols = img.shape[:2]
    
    M = cv2.getRotationMatrix2D((cols/2,rows/2), degrees, 1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    return dst

#%% Log transformation

def log_transformation(img, c) :
    img = uint_to_float(img)
    res = c * np.log(1 + img)
    return saturation(res)

#%%

def createWhiteDisk2(height=100, width=100, xc=50, yc=50, rc=20):
    xx, yy = np.meshgrid(range(height), range(width))
    img = np.array(
        ((xx - xc) ** 2 + (yy - yc) ** 2 - rc ** 2) < 0).astype('float64')
    return img
    