# -*- coding: utf-8 -*-

#%%
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
from random import randint
from pdi_lib import *
def nop() :
    pass

def createWhiteDisk2(height=100, width=100, xc=50, yc=50, rc=20):
    xx, yy = np.meshgrid(range(height), range(width))
    img = np.array(
        ((xx - xc) ** 2 + (yy - yc) ** 2 - rc ** 2) < 0).astype('float64')
    return img
    
#%% Q1

cv2.namedWindow("tracker_img", cv2.WINDOW_KEEPRATIO)


img = np.ones((100,100), np.float64)

r = 100
c = 100

xx, yy = np.meshgrid(np.linspace(0, c-1, c), np.linspace(0, r-1, r))

deg = 0
cv2.createTrackbar("choose_deg", "tracker_img", deg, 360, nop)

while True :
    deg = cv2.getTrackbarPos("choose_deg", "tracker_img"); deg = np.deg2rad(deg)
    img_to_plot = xx * np.cos(deg) - yy*np.sin(deg)
    cv2.normalize(img_to_plot, img_to_plot, 0, 1, cv2.NORM_MINMAX)
    cv2.imshow("img_original", img)
    cv2.imshow("tracker_img", img_to_plot)
    if cv2.waitKey(1) == ord('q'):
        break
cv2.destroyAllWindows()

#%% Q2

img = np.zeros((500,500), np.uint8) != 0

for i in range(30) :
    raio = np.random.randint(5,50)
    xc = np.random.randint(0,500)
    yc = np.random.randint(0,500)
    circle = createWhiteDisk2(500, 500, xc, yc, raio) != 0
    img = or_img(circle, img)
plt.imshow(img, 'gray')
plt.show()

#%% Q4


            




        




