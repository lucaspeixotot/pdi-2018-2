# -*- coding: utf-8 -*-

#%%
import cv2
import numpy as np
import matplotlib.pyplot as plt

def do_nothing(x):
    pass


def create2DGaussian(rows=100,
                     cols=100,
                     mx=50,
                     my=50,
                     sx=10,
                     sy=100,
                     theta=0):
    xx0, yy0 = np.meshgrid(range(rows), range(cols))
    xx0 -= mx
    yy0 -= my
    theta = np.deg2rad(theta)
    xx = xx0 * np.cos(theta) - yy0 * np.sin(theta)
    yy = xx0 * np.sin(theta) + yy0 * np.cos(theta)
    try:
        img = np.exp(- ((xx ** 2) / (2 * sx ** 2) +
                        (yy ** 2) / (2 * sy ** 2)))
    except ZeroDivisionError:
        img = np.zeros((rows, cols), dtype='float64')

    cv2.normalize(img, img, 1, 0, cv2.NORM_MINMAX)
    return img
    
def get_color_img(hsv, img, color):
    if color == "red": 
        lower = np.array([169, 100, 100])
        upper = np.array([189, 255, 255])
    elif color == "yellow":
        lower = np.array([20, 100, 100])
        upper = np.array([30, 255, 255])
    elif color == "green":
        lower = np.array([45,60,60])
        upper = np.array([80,255,255])
    elif color == "blue":
        lower = np.array([110,50,50])
        upper = np.array([130,255,255])
    elif color == "orange":
        lower = np.array([0,70,50])
        upper = np.array([10,255,255])
    else:
        return None
        

    mask = cv2.inRange(hsv, lower, upper)
    return cv2.bitwise_and(img, img, mask = mask)

#%% Primeira questão

background = np.zeros((400, 400), dtype=np.float32)

radius = int(background.shape[0] * 0.7)
center = (int(background.shape[0] / 2), int(background.shape[1] / 2))

v = np.copy(background)
disk = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (radius, radius))
v[int(center[0] - radius / 2):int(center[0] + radius / 2),
int(center[1] - radius / 2):int(center[1] + radius / 2)] = disk

s = np.float32(create2DGaussian(background.shape[0], background.shape[1], center[0], center[1], 90, 90))
s = v * s

h = np.copy(background)

for theta in np.arange(0, np.pi * 2, np.pi * 2 / 300):
    for r in range(0, int(radius / 2)):
        h[int(center[0] - r * np.sin(theta))][int(center[1] - r * np.cos(theta))] = theta / 9

h = cv2.morphologyEx(h, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))

h = np.uint8(255 * h)
s = np.uint8(255 * s)
v = np.uint8(255 * v)


hsv = cv2.merge((h, s, v))
bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
cv2.imshow("BGR", bgr)

cv2.waitKey(0)
cv2.destroyAllWindows()

#%% Segunda questão

img = cv2.imread('imgs/chips.png')
hsv_image = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

red_img = get_color_img(hsv_image, img, "red")
blue_img = get_color_img(hsv_image, img, "blue")
yellow_img = get_color_img(hsv_image, img, "yellow")
green_img = get_color_img(hsv_image, img, "green")
orange_img = get_color_img(hsv_image, img, "orange")
plt.subplot(231);plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));plt.title('Original')
plt.subplot(232);plt.imshow(cv2.cvtColor(red_img, cv2.COLOR_BGR2RGB));plt.title('Red image')
plt.subplot(233);plt.imshow(cv2.cvtColor(green_img, cv2.COLOR_BGR2RGB));plt.title('Green image')
plt.subplot(234);plt.imshow(cv2.cvtColor(yellow_img, cv2.COLOR_BGR2RGB));plt.title('Yellow image')
plt.subplot(235);plt.imshow(cv2.cvtColor(orange_img, cv2.COLOR_BGR2RGB));plt.title('Orange image')
plt.subplot(236);plt.imshow(cv2.cvtColor(blue_img, cv2.COLOR_BGR2RGB));plt.title('Blue image')

#%% Terceira questão

img_orig = cv2.imread('imgs/horse.png', cv2.IMREAD_GRAYSCALE)

ret,img = cv2.threshold(~img_orig,127,255,0)

size = np.size(img)
skel = np.zeros(img.shape,np.uint8)


element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
done = False
 
while( not done):
    eroded = cv2.erode(img,element)
    temp = cv2.dilate(eroded,element)
    temp = cv2.subtract(img,temp)
    skel = cv2.bitwise_or(skel,temp)
    img = eroded.copy()
 
    zeros = size - cv2.countNonZero(img)
    if zeros==size:
        done = True

plt.subplot(121), plt.imshow(~img_orig, cmap='gray'), plt.title('Original')
plt.subplot(122), plt.imshow(skel, cmap='gray'), plt.title('Skeleton')



#%% Quarta questão

img = cv2.imread('imgs/retinal.png', cv2.IMREAD_GRAYSCALE)

kernel = np.ones((3,3),np.uint8)

erode = cv2.erode(img, kernel, iterations = 1)
dilate = cv2.dilate(img, kernel, iterations = 1)

gradient = dilate - erode

plt.subplot(121), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(122), plt.imshow(gradient, cmap='gray'), plt.title('Gradient')

