# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 09:48:26 2019

@author: alunoic
"""
#%%
import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread('imgs/baboon.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
r = np.uint8([1, 0, 0])
g = np.uint8([0, 1, 0])
b = np.uint8([0, 0, 1])
c = np.uint8([0, 1, 1])
m = np.uint8([1, 0, 1])
y = np.uint8([1, 1, 0])

plt.subplot('241'); plt.title('RGB');plt.imshow(img)
plt.subplot('242'); plt.title('R');plt.imshow(r*img)
plt.subplot('243'); plt.title('G');plt.imshow(g*img)
plt.subplot('244'); plt.title('B');plt.imshow(b*img)
plt.subplot('245'); plt.title('C');plt.imshow(c*img)
plt.subplot('246'); plt.title('M');plt.imshow(m*img)
plt.subplot('247'); plt.title('Y');plt.imshow(y*img)

#%%

image = cv2.imread('imgs/baboon.png')

b = image.copy()
# set green and red channels to 0
b[:, :, 1] = 0
b[:, :, 2] = 0


g = image.copy()
# set blue and red channels to 0
g[:, :, 0] = 0
g[:, :, 2] = 0

r = image.copy()
# set blue and green channels to 0
r[:, :, 0] = 0
r[:, :, 1] = 0


# RGB - Blue
cv2.imshow('B-RGB', b)

# RGB - Green
cv2.imshow('G-RGB', g)

# RGB - Red
cv2.imshow('R-RGB', r)

cv2.waitKey(0)