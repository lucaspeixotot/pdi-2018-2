#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 09:29:19 2019

@author: igortheotonio
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import reconstruction

img = cv2.imread('img/Fig0930(a)(calculator).tif', cv2.IMREAD_GRAYSCALE)
kernel = np.ones((1,71),np.uint8)
kernel1 = np.ones((1,11),np.uint8)
kernel2 = np.ones((1, 21), np.uint8)

img_erosion = cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel) # marker, img = mask
img_opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

img_opening_by_reconstruction = reconstruction(img_erosion, img)
img_top_hat_by_reconstruction = img - img_opening_by_reconstruction
img_top_hat = img - img_opening
img_top_hat_by_reconstruction_erosion = cv2.morphologyEx(img_top_hat_by_reconstruction, cv2.MORPH_ERODE, kernel1)
img_top_hat_opening_by_reconstruction = reconstruction(img_top_hat_by_reconstruction_erosion, img_top_hat)
img_top_hat_opening_by_reconstruction_dilatation = cv2.morphologyEx(img_top_hat_opening_by_reconstruction, cv2.MORPH_DILATE, kernel2)
minimum = np.minimum(img_top_hat_by_reconstruction, img_top_hat_opening_by_reconstruction_dilatation)
final = reconstruction(minimum, img_top_hat_opening_by_reconstruction_dilatation)

plt.subplot(331), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(332), plt.imshow(img_opening_by_reconstruction, cmap='gray'), plt.title('Opening by reconstruction')
plt.subplot(333), plt.imshow(img_opening, cmap='gray'), plt.title('Opening')
plt.subplot(334), plt.imshow(img_top_hat_by_reconstruction, cmap='gray'), plt.title('Top-Hat by reconstruction')
plt.subplot(335), plt.imshow(img_top_hat, cmap='gray'), plt.title('Top-Hat')
plt.subplot(336), plt.imshow(img_top_hat_opening_by_reconstruction, cmap='gray'), plt.title('Top-Hat opening by reconstruction')
plt.subplot(337), plt.imshow(img_top_hat_opening_by_reconstruction_dilatation , cmap='gray'), plt.title('Top-Hat opening by reconstruction dilatation')
plt.subplot(338), plt.imshow(minimum, cmap='gray'), plt.title('Minimum d and g')
plt.subplot(339), plt.imshow(final, cmap='gray'), plt.title('Final')
plt.show()


