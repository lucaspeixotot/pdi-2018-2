# -*- coding: utf-8 -*-

#%%
import cv2
import numpy as np
import matplotlib.pyplot as plt

def doNothing() :
    pass

folder = "assunto-prova-2/prova2/"
#%%
img = cv2.imread('/home/alunoic/pdi-2018-2/assunto-prova-2/prova2/5.png', cv2.IMREAD_COLOR)
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

lower_hue = 0
lower_saturation = 0
lower_value = 0
upper_hue = 255
upper_saturation = 255
upper_value = 255

cv2.namedWindow("img")
cv2.namedWindow("img2")
cv2.namedWindow("img3")

cv2.createTrackbar("lh", "img2", lower_hue, 255, doNothing)
cv2.createTrackbar("uh", "img2", upper_hue, 255, doNothing)
cv2.createTrackbar("ls", "img2", lower_saturation, 255, doNothing)
cv2.createTrackbar("us", "img2", upper_saturation, 255, doNothing)
cv2.createTrackbar("lv", "img2", lower_value, 255, doNothing)
cv2.createTrackbar("uv", "img2", upper_value, 255, doNothing)

while cv2.waitKey(1) != ord('q'):
    lower_hue = cv2.getTrackbarPos("lh", "img2")
    lower_saturation = cv2.getTrackbarPos("ls", "img2")
    lower_value = cv2.getTrackbarPos("lv", "img2")
    upper_hue = cv2.getTrackbarPos("uh", "img2")
    upper_saturation = cv2.getTrackbarPos("us", "img2")
    upper_value = cv2.getTrackbarPos("uv", "img2")

    mask1 = (lower_hue , lower_saturation, lower_value)
    print(mask1)
    mask2 = (upper_hue, upper_saturation, upper_value)
    mask = cv2.inRange(hsv, mask1, mask2)

    img2 = cv2.bitwise_and(img, img, mask=mask)
    cv2.imshow("img", img)
    cv2.imshow("img2", img2)
    cv2.imshow("img3", img-img2)
cv2.destroyAllWindows()
