# -*- coding: utf-8 -*-

import cv2
import numpy as np
import matplotlib.pyplot as plt

folder = "assunto-prova-2/prova2/"

#%% primeira questão

img = cv2.imread(folder + '1.png', cv2.IMREAD_GRAYSCALE)
dilation = cv2.dilate(img, kernel, iterations = 1)
erosion = cv2.erode(dilation, kernel, iterations = 1)
plt.subplot(131), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(dilation, cmap='gray'), plt.title('Dilation')
plt.subplot(133), plt.imshow(erosion, cmap='gray'), plt.title('Dilation + Erosion')