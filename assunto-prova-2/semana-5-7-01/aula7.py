# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 09:59:35 2019

@author: alunoic
"""

#%%
from pdi_lib import *

#%%
img = cv2.imread('imgs/rectangle.jpg',0)
f = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
ftype = cv2.split(f)
fshift = np.fft.fftshift(f)
mag_f = 20*np.log(cv2.magnitude(f[:,:,0],f[:,:,1]))
mag_shift = 20*np.log(cv2.magnitude(fshift[:,:,0],fshift[:,:,1]))
img_log = log_transformation(real, 1)
img_log_mag = 20*np.log(np.abs(img_log))

#%%
'''
plt.subplot(221),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
'''

plt.subplot(221),plt.imshow(ftype[0], cmap = 'gray')
plt.title('Magnitude Spectrum fft'), plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(ftype[1], cmap = 'gray')
plt.title('Magnitude Spectrum fft shift'), plt.xticks([]), plt.yticks([])

plt.subplot(223),plt.imshow(mag_f, cmap = 'gray')
plt.title('Real'), plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(mag_shift, cmap = 'gray')
plt.title('Imag'), plt.xticks([]), plt.yticks([])
plt.show()