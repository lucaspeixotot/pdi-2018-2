# -*- coding: utf-8 -*-

#%% imports
import numpy as np
import cv2
from pdi_lib import *

def doNothing(param):
    pass

def createWhiteDisk2(height = 100, width = 100, xc = 50, yc = 50, rc = 20):
        xx, yy = np.meshgrid( range(height), range(width) )
        img = np.array( (xx-xc)**2 + (yy-yc)**2 - rc**2 < 0 ).astype('float64')
        return img
        
     
def createCosineImage2(height = 100, width = 100, freq = 0, theta = 0):
    img = np.zeros((height,width), dtype=np.float64)
    xx, yy = np.meshgrid( range(height), range(width) )
    theta = np.deg2rad(theta)
    rho = xx * np.cos(theta) - yy * np.sin(theta)
    img[:] = np.cos( 2 * np.pi * freq * rho )
    return img

def scaleImage2_uchar(src):
	tmp = np.copy(src)
	if src.dtype != np.float32:
		tmp = np.float32(tmp)
	cv2.normalize(tmp, tmp, 1, 0, cv2.NORM_MINMAX)
	tmp = 255*tmp
	tmp = np.uint8(tmp)
	return tmp

def create2DGaussian(rows = 100, cols = 100, mx = 50, my = 50, sx = 10, sy = 100, theta = 0):
	xx0, yy0 = np.meshgrid(range(rows), range(cols))
	xx0 -= mx
	yy0 -= my
	theta = np.deg2rad(theta)
	xx = xx0 * np.cos(theta) - yy0 * np.sin(theta)
	yy = xx0 * np.sin(theta) + yy0 * np.cos(theta)
	try:
		img = np.exp( -(xx**2)/(2*sx**2) + (yy**2)/(2*sy**2) )
	except Exception as e:
		img = np.zeros((rows,cols), dtype='float64')
	cv2.normalize(img,img,1,0, cv2.NORM_MINMAX)
	return img
    
#%% Creating white disk
white_disk = createWhiteDisk2(400,400, 200, 200)     


#%% White disk domain frequency
img = white_disk

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()


#%% Creating black disk
black_disk = 1 - white_disk


#%% black disk domain frequency
img = black_disk

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()



#%% Creating two sines

sin1 = createCosineImage2(400,400, 50, -45)     
sin2 = createCosineImage2(400,400, 500, 45)
sin_total = saturation(sin1 + sin2)


#%% sines domain frequency
img = sin_total

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()

#%% Creating a gaussian

gaussian = create2DGaussian(400, 400, 200, 200)     


#%% gaussian domain frequency
img = gaussian

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()

#%% white ring
white_ring = createWhiteDisk2(400,400, 200, 200, 50).astype(np.int32)
mask = 1 - createWhiteDisk2(400,400, 200, 200, 40).astype(np.int32)
white_ring = (white_ring & mask).astype(np.float32)


#%% white_ring domain frequency
img = white_ring

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()

#%% black ring

black_ring = 1 - white_ring


#%% black_ring domain frequency
img = black_ring

dft = cv2.dft(np.float32(img),flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(dft)

magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()

