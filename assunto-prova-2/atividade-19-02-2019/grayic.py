#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 15:42:19 2019

@author: lucas
"""

#%%
img = cv2.imread('logo-ic.png', cv2.IMREAD_GRAYSCALE)
plt.imshow(img, cmap='gray')
plt.show()

#%%
cv2.imshow("img", img)
cv2.waitKey(0)
cv2.destroyAllWindows()