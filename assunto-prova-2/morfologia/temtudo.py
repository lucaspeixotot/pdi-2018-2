# -*- coding: utf-8 -*-

#%%
import numpy as np
import matplotlib.pyplot as plt
import cv2

#%%
img = cv2.imread('imgs/j.png', cv2.IMREAD_GRAYSCALE)


#%% dilation

kernel = np.ones((3,3),np.uint8)
imgd3x3 = cv2.dilate(img,kernel,iterations = 1)
kernel = np.ones((5,5),np.uint8)
imgd5x5 = cv2.dilate(img,kernel,iterations = 1)
imgd2it = cv2.dilate(img,kernel,iterations = 2)
imgd5it = cv2.dilate(img,kernel,iterations = 5)



#%% plotting dilation

plt.subplot(231), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(232), plt.imshow(imgd3x3, cmap='gray'), plt.title('Dilated 3x3')
plt.subplot(233), plt.imshow(imgd5x5, cmap='gray'), plt.title('Dilated 5x5')
plt.subplot(234), plt.imshow(imgd2it, cmap='gray'), plt.title('Dilated 2it')
plt.subplot(235), plt.imshow(imgd5it, cmap='gray'), plt.title('Dilated 5it')

#%% eroding and plotting

imgerod = cv2.erode(imgd5x5,kernel,iterations = 1)
plt.subplot(231), plt.imshow(imgd5x5, cmap='gray'), plt.title('Dilated 5x5')
plt.subplot(232), plt.imshow(imgerod, cmap='gray'), plt.title('eroded')
plt.subplot(233), plt.imshow(img, cmap='gray'), plt.title('original')

#%% removendo ruidos - opening
img = cv2.imread('imgs/j_salt.png', cv2.IMREAD_GRAYSCALE)
erosion = cv2.erode(img, kernel, iterations = 1)
dilation = cv2.dilate(erosion, kernel, iterations = 1)
plt.subplot(131), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(erosion, cmap='gray'), plt.title('Erosion')
plt.subplot(133), plt.imshow(dilation, cmap='gray'), plt.title('Erosion + Dilation')

#%% removendo ruídos - closing

img = cv2.imread('imgs/j_pepper.png', cv2.IMREAD_GRAYSCALE)
dilation = cv2.dilate(img, kernel, iterations = 1)
erosion = cv2.erode(dilation, kernel, iterations = 1)
plt.subplot(131), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(dilation, cmap='gray'), plt.title('Dilation')
plt.subplot(133), plt.imshow(erosion, cmap='gray'), plt.title('Dilation + Erosion')

#%% removing components
img = cv2.imread('imgs/ww.png', cv2.IMREAD_GRAYSCALE)
kernel = np.ones((5,5),np.uint8)
erosion = cv2.erode(img, kernel, iterations = 11)
dilation = cv2.dilate(erosion, kernel, iterations = 6)
plt.subplot(131), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(erosion, cmap='gray'), plt.title('Erosion')
plt.subplot(133), plt.imshow(dilation, cmap='gray'), plt.title('Erosion + Dilation')

#%% getting boundary
img = cv2.imread('imgs/lincoln.png', cv2.IMREAD_GRAYSCALE)
kernel = np.ones((3,3),np.uint8)
erode_img = cv2.erode(img, kernel, iterations = 1)
boundary = img - erode_img
plt.subplot(121), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(122), plt.imshow(boundary, cmap='gray'), plt.title('Boundary')

#%% hole filling

img = cv2.imread("imgs/region-filling-reflections.tif", cv2.IMREAD_GRAYSCALE)
kernel = np.ones((25,25),np.uint8)
dilated = cv2.dilate(img, kernel, iterations = 1)
eroded = cv2.erode(dilated, kernel, iterations = 2)
plt.subplot(131), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(dilated, cmap='gray'), plt.title('Dilation')
plt.subplot(133), plt.imshow(eroded, cmap='gray'), plt.title('Dilation + Erosion')

#%% noise finger

img = cv2.imread("imgs/noisy-fingerprint.png", cv2.IMREAD_GRAYSCALE)

kernel = np.ones((3,3),np.uint8)
eroded = cv2.erode(img, kernel, iterations = 1)
openning = cv2.dilate(eroded, kernel, iterations = 1)
dilate_more = cv2.dilate(openning, kernel, iterations = 1)
closing = cv2.erode(dilate_more, kernel, iterations = 1)
plt.subplot(231), plt.imshow(img, cmap='gray'), plt.title('Original')
plt.subplot(232), plt.imshow(eroded, cmap='gray'), plt.title('Eroded')
plt.subplot(233), plt.imshow(openning, cmap='gray'), plt.title('Openning')
plt.subplot(234), plt.imshow(dilate_more, cmap='gray'), plt.title('Dilate more')
plt.subplot(235), plt.imshow(closing, cmap='gray'), plt.title('Openning + closing')

#%% convex hull open hand

img_open = cv2.imread("imgs/hand.png", cv2.IMREAD_GRAYSCALE)
blur = cv2.blur(img_open, (3, 3)) # blur the image
ret, thresh = cv2.threshold(blur, 50, 255, cv2.THRESH_BINARY)
#plt.subplot(131), plt.imshow(img_open, cmap='gray'), plt.title('Original')
#plt.subplot(132), plt.imshow(blur, cmap='gray'), plt.title('Blur')
#plt.subplot(133), plt.imshow(thresh, cmap='gray'), plt.title('Thresh')

#%% convex hull continuation open hand

contours_open, hierarchy_open = cv2.findContours(img_open.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# create hull array for convex hull points
hull_open = []
 
# calculate points for each contour
for i in range(len(contours_open)):
    # creating convex hull object for each contour
    hull_open.append(cv2.convexHull(contours_open[i], False))
    
# create an empty black image
img_open_hull = np.zeros((img_open.shape[0], img_open.shape[1], 3), np.uint8)
open_hull_img = np.zeros((img_open.shape[0], img_open.shape[1], 3), np.uint8)
 
# draw contours and hull points
for i in range(len(contours_open)):
    color_contours = (0, 255, 0) # green - color for contours
    color = (255, 0, 0) # blue - color for convex hull
    # draw ith contour
    cv2.drawContours(img_open_hull, contours_open, i, color_contours, 1, 8, hierarchy_open)
    # draw ith convex hull object
    cv2.drawContours(img_open_hull, hull_open, i, color, 1, 8)

    cv2.drawContours(open_hull_img, hull_open, i, color, 1, 8)
    
#%% plotting convex hull open hand
plt.subplot(131), plt.imshow(img_open, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(open_hull_img, cmap='gray'), plt.title('Convex hull')
plt.subplot(133), plt.imshow(img_open_hull, cmap='gray'), plt.title('Convex hull and hand')

#%% convex hull close hand

img_close = cv2.imread("imgs/hand2.jpg", cv2.IMREAD_GRAYSCALE)
#blur = cv2.blur(img_close, (3, 3)) # blur the image
#ret, thresh = cv2.threshold(blur, 50, 255, cv2.THRESH_BINARY)
plt.subplot(131), plt.imshow(img_close, cmap='gray'), plt.title('Original')
#plt.subplot(132), plt.imshow(blur, cmap='gray'), plt.title('Blur')
#plt.subplot(133), plt.imshow(thresh, cmap='gray'), plt.title('Thresh')

#%% convex hull continuation close hand

contours_close, hierarchy_close = cv2.findContours(img_close, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# create hull array for convex hull points
hull_close = []
 
# calculate points for each contour
for i in range(len(contours_close)):
    # creating convex hull object for each contour
    hull_close.append(cv2.convexHull(contours_close[i], False))
    
# create an empty black image
img_close_hull = np.zeros((img_close.shape[0], img_close.shape[1], 3), np.uint8)
close_hull_img = np.zeros((img_close.shape[0], img_close.shape[1], 3), np.uint8)
 
# draw contours and hull points
for i in range(len(contours_close)):
    color_contours = (0, 255, 0) # green - color for contours
    color = (255, 0, 0) # blue - color for convex hull
    # draw ith contour
    cv2.drawContours(img_close_hull, contours_close, i, color_contours, 1, 8, hierarchy_close)
    # draw ith convex hull object
    cv2.drawContours(img_close_hull, hull_close, i, color, 1, 8)

    cv2.drawContours(close_hull_img, hull_close, i, color, 1, 8)
    
#%% plotting convex hull close hand
plt.subplot(131), plt.imshow(img_close, cmap='gray'), plt.title('Original')
plt.subplot(132), plt.imshow(close_hull_img, cmap='gray'), plt.title('Convex hull')
plt.subplot(133), plt.imshow(img_close_hull, cmap='gray'), plt.title('Convex hull and hand')


#%% solidity cloase vs open
cac= cv2.contourArea(contours_close[1])
huc = cv2.contourArea(hull_close[1])
solidity_close = float(cac)/huc

cao = cv2.contourArea(contours_open[0])
huo = cv2.contourArea(hull_open[0])
solidity_open = float(cao)/huo

print("------------------")
print("Solidity open: %f\n" % solidity_open)
print("Solidity close: %f\n" % solidity_close)
print("------------------")

#%% skeleton
import cv2
import numpy as np
 
img_orig = cv2.imread('imgs/horse2.png',0)
ret,img = cv2.threshold(img_orig,127,255,0)
size = np.size(img)
skel = np.zeros(img.shape,np.uint8)


element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
done = False
 
while( not done):
    eroded = cv2.erode(img,element)
    temp = cv2.dilate(eroded,element)
    temp = cv2.subtract(img,temp)
    skel = cv2.bitwise_or(skel,temp)
    img = eroded.copy()
 
    zeros = size - cv2.countNonZero(img)
    if zeros==size:
        done = True

plt.subplot(121), plt.imshow(img_orig, cmap='gray'), plt.title('Original')
plt.subplot(122), plt.imshow(skel, cmap='gray'), plt.title('Skeleton')

#%% top-hat, gradient, blackhat
img = cv2.imread("imgs/j.png",cv2.IMREAD_GRAYSCALE)
kernel = np.ones((3,3),np.uint8)

erode = cv2.erode(img, kernel, iterations = 1)
dilate = cv2.dilate(img, kernel, iterations = 1)
openning = cv2.dilate(erode, kernel, iterations = 1)
closing = cv2.erode(dilate, kernel, iterations = 1)


tophat = img - openning
blackhat = closing - img
gradient = dilate - erode

plt.subplot(131), plt.imshow(tophat, cmap='gray'), plt.title('Tophat')
plt.subplot(132), plt.imshow(blackhat, cmap='gray'), plt.title('Blackhat')
plt.subplot(133), plt.imshow(gradient, cmap='gray'), plt.title('Gradient')



"""
# Rectangular Kernel
>>> cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
array([[1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1]], dtype=uint8)

# Elliptical Kernel
>>> cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
array([[0, 0, 1, 0, 0],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [0, 0, 1, 0, 0]], dtype=uint8)

# Cross-shaped Kernel
>>> cv2.getStructuringElement(cv2.MORPH_CROSS,(5,5))
array([[0, 0, 1, 0, 0],
       [0, 0, 1, 0, 0],
       [1, 1, 1, 1, 1],
       [0, 0, 1, 0, 0],
       [0, 0, 1, 0, 0]], dtype=uint8)
"""

