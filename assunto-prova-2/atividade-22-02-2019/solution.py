# -*- coding: utf-8 -*-

#%%
from pdi_lib import *

def get_color_img(hsv, img, color):
    if color == "red": 
        lower = np.array([169, 100, 100])
        upper = np.array([189, 255, 255])
        mask = cv2.inRange(hsv, lower, upper)
        lower = np.array([0, 100, 100])
        upper = np.array([20, 255, 255])
    elif color == "yellow":
        lower = np.array([20, 100, 100])
        upper = np.array([30, 255, 255])
    elif color == "green":
        lower = np.array([45,60,60])
        upper = np.array([80,255,255])
    elif color == "blue":
        lower = np.array([110,50,50])
        upper = np.array([130,255,255])
    elif color == "orange":
        lower = np.array([0,70,50])
        upper = np.array([10,255,255])
    else:
        return None
        
    if color == "red":
        mask = mask | cv2.inRange(hsv, lower, upper)
    else:
        mask = cv2.inRange(hsv, lower, upper)

    return cv2.bitwise_and(img, img, mask = mask)

#%% getting image

img = cv2.imread('imgs/chips.png')
hsv_image = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
h, s, v = cv2.split(hsv_image)

#%% getting hsv histogram
hist_h = cv2.calcHist([hsv_image], [0], None, [256], [0, 256])
hist_s = cv2.calcHist([hsv_image], [1], None, [256], [0, 256])
hist_v = cv2.calcHist([hsv_image], [2], None, [256], [0, 256])

#%% plotting hsv channels

plt.subplot(221);plt.imshow(h, cmap='gray');plt.title('H channel')
plt.subplot(222);plt.imshow(s, cmap='gray');plt.title('S channel')
plt.subplot(223);plt.imshow(v, cmap='gray');plt.title('V channel')
plt.subplot(224);plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));plt.title('Original')
plt.show()

#%% segmenting colors

red_img = get_color_img(hsv_image, img, "red")
blue_img = get_color_img(hsv_image, img, "blue")
yellow_img = get_color_img(hsv_image, img, "yellow")
green_img = get_color_img(hsv_image, img, "green")
orange_img = get_color_img(hsv_image, img, "orange")
plt.subplot(231);plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));plt.title('Original')
plt.subplot(232);plt.imshow(cv2.cvtColor(red_img, cv2.COLOR_BGR2RGB));plt.title('Red image')
plt.subplot(233);plt.imshow(cv2.cvtColor(green_img, cv2.COLOR_BGR2RGB));plt.title('Green image')
plt.subplot(234);plt.imshow(cv2.cvtColor(yellow_img, cv2.COLOR_BGR2RGB));plt.title('Yellow image')
plt.subplot(235);plt.imshow(cv2.cvtColor(orange_img, cv2.COLOR_BGR2RGB));plt.title('Orange image')
plt.subplot(236);plt.imshow(cv2.cvtColor(blue_img, cv2.COLOR_BGR2RGB));plt.title('Blue image')


#%% plottting hsv hsitograms

plt.plot(hist_h)
plt.plot(hist_s)
plt.plot(hist_v)
plt.xlim([0, 256])
plt.title('HSV Histogram')
plt.legend(['H histogram', 'S histogram', 'V histogram'])
plt.show()
